require('../config').searchENV();
const database = require('../database');
const logger = require('./logger');
const Promise = require('bluebird');
const collections = require('../database/collections.json');

database.connect()
.then((db) => {
  const createdCollections = Object.keys(collections)
  .map((key) => {
    const collection = collections[key];
    logger.log(`Create the new collection : ${collection.name}`);
    logger.log(`About this collection : ${collection.description}`);
    return db.createCollection(collection.name);
  });
  Promise.all(createdCollections).then(() => {
    logger.log('The database is successfuly initialized');
    database.close();
  });
})
.catch();
