require('../config').searchENV();
const database = require('../database');
const logger = require('./logger');
const Promise = require('bluebird');
const collections = require('../database/collections.json');

database.connect()
.then((db) => {
  const promises = Object.keys(collections)
  .map((key) => {
    const collection = collections[key];
    logger.log(`Load fixtures for this collection : ${collection.name}`);
    const fixtures = require(`../database/fixtures/${collection.name}`);
    logger.log(`Insert : ${JSON.stringify(fixtures.getAll(), null, 2)}`);
    return db.collection(collection.name).insertMany(fixtures.getAll());
  });
  Promise.all(promises).then(() => {
    logger.log('The database is successfuly initialized');
    database.close();
  });
})
.catch();
