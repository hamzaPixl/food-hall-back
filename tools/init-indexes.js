require('../config').searchENV();
const database = require('../database');
const logger = require('./logger');
const Promise = require('bluebird');
const collections = require('../database/collections.json');

database.connect()
.then((db) => {
  const createdIndexes = Object.keys(collections)
  .map((key) => {
    const collection = collections[key];
    logger.log(`Create indexes for this collection : ${collection.name}`);
    logger.log(`Indexes : ${JSON.stringify(collection.indexes, null, 2)}`);
    return db.collection(collection.name).createIndexes(collection.indexes);
  });
  Promise.all(createdIndexes)
  .then(() => {
    logger.log('The database is successfuly initialized');
    database.close();
  })
  .catch((err) => {
    logger.log(err);
    database.close();
  });
})
.catch();
