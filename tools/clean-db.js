require('../config').searchENV();
const database = require('../database');
const logger = require('./logger');
const Promise = require('bluebird');

database.connect()
.then((db) => {
  db.listCollections()
  .toArray()
  .then((list) => {
    const cleanCollections = list.map((collection) => {
      logger.log(`This collection will be empty : ${collection.name}`);
      return db.collection(collection.name).deleteMany({});
    });
    const deleteIndexes = list.map((collection) => {
      logger.log(`This collection will be remove his indexes : ${collection.name}`);
      return db.collection(collection.name).dropIndex('*');
    });
    Promise.all([cleanCollections, deleteIndexes]).then(() => {
      logger.log('The database is successfuly cleared');
      database.close();
    });
  });
})
.catch();
