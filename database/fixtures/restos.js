const faker = require('faker');

/**
 * Get All fixtures for this collection
 */
function getAll() {
  const fixtures = [];
  for (let index = 0; index < 10; index += 1) {
    const payload = {
      id: faker.random.uuid(),
      tel: faker.phone.phoneNumber(),
      description: faker.lorem.paragraph(),
      title: faker.lorem.words(),
      logo: faker.internet.avatar(),
      likes: faker.random.number(),
      views: faker.random.number(),
      loc: { type: 'Point', coordinates: [parseFloat(faker.address.longitude()), parseFloat(faker.address.latitude())] },
      priceRange: '$$$',
      paymentMethod: [{ type: 'cash' }, { type: 'sodexo' }, { type: 'bancontact' }, { type: 'visa' }],
      address: {
        street: faker.address.streetAddress(),
        number: faker.random.number(),
        city: faker.address.city(),
        zip: faker.address.zipCode(),
        country: faker.address.country(),
      },
      categories: [
        {
          name: faker.random.word(),
        },
        {
          name: faker.random.word(),
        },
        {
          name: faker.random.word(),
        },
      ],
      socialMedias: [
        {
          source: faker.internet.domainWord(),
          link: faker.internet.url(),
        },
        {
          source: faker.internet.domainWord(),
          link: faker.internet.url(),
        },
      ],
      openDays: [
        {
          day: faker.lorem.word(),
          openTime: '8:00 - 15:00',
        },
        {
          day: faker.lorem.word(),
          openTime: '8:00 - 18:00',
        },
        {
          day: faker.lorem.word(),
          openTime: '8:00 - 19:00',
        },

      ],
      createdAt: faker.date.recent(),
      updatedAt: faker.date.recent(),
    };
    fixtures.push(payload);
  }
  return fixtures;
}

module.exports = { getAll };
