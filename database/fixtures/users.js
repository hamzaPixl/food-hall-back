const faker = require('faker');

/**
 * Get All fixtures for this collection
 */
function getAll() {
  const fixtures = [];
  for (let index = 0; index < 10; index += 1) {
    const payload = {
      id: faker.random.uuid(),
      tel: faker.phone.phoneNumber(),
      email: faker.internet.email(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      lastName: faker.name.lastName(),
      firstName: faker.name.firstName(),
      favoris: [faker.random.uuid(), faker.random.uuid(), faker.random.uuid(), faker.random.uuid()],
      address: {
        street: faker.address.streetAddress(),
        number: faker.random.number(),
        city: faker.address.city(),
        zip: faker.address.zipCode(),
        country: faker.address.country(),
      },
    };
    fixtures.push(payload);
  }
  return fixtures;
}

module.exports = { getAll };
