const RepositoryController = require('../../../database/RepositoryController');
const restoFactory = require('../factories/restoFactory');

const COLLECTION = 'restos';

function get(query, db) {
  const repositoryController = new RepositoryController(db, COLLECTION);
  if (Object.keys(query).length) {
    return repositoryController.findByValues(query)
    .then(restoFactory.createFromDbPayloadMap);
  }
  return repositoryController.findAll()
  .then(restoFactory.createFromDbPayloadMap);
}

function getById(restoId, db) {
  const repositoryController = new RepositoryController(db, COLLECTION);
  return repositoryController.findById(restoId)
  .then(restoFactory.createFromDbPayloadMap);
}

function getAround(coord, db) {
  const repositoryController = new RepositoryController(db, COLLECTION);
  const query = {
    lat: parseFloat(coord.lat),
    lon: parseFloat(coord.lon),
    limit: parseFloat(coord.limit),
    radius: 6377.83027,
  };
  const queryDb = { loc: { $geoWithin: { $centerSphere: [[query.lon, query.lat], query.limit / query.radius] } } };
  return repositoryController.findByValues(queryDb)
  .then(restoFactory.createFromDbPayloadMap);
}

module.exports = {
  get,
  getById,
  getAround,
};
