const Address = require('./Address');
const Loc = require('./Loc');
const moment = require('moment');

class Resto {
  constructor() {
    this.id = null;
    this.description = null;
    this.title = null;
    this.logo = null;
    this.likes = null;
    this.views = null;
    this.loc = new Loc();
    this.priceRange = null;
    this.tel = null;
    this.address = new Address();
    this.categories = [];
    this.socialMedias = [];
    this.openDays = [];
    this.createdAt = moment.utc();
    this.updatedAt = moment.utc();
  }
}

module.exports = Resto;
