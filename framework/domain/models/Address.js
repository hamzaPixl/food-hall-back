class Address {
  constructor() {
    this.street = null;
    this.number = null;
    this.city = null;
    this.zip = null;
    this.country = null;
  }
}

module.exports = Address;
