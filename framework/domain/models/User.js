const Address = require('./Address');

class User {
  constructor() {
    this.id = null;
    this.firstName = null;
    this.lastName = null;
    this.email = null;
    this.tel = null;
    this.address = new Address();
    this.favoris = null;
    this.password = null;
    this.username = null;
  }
}

module.exports = User;
