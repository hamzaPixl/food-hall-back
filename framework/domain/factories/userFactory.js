const User = require('../models/User');
const addressFactory = require('./addressFactory');

function createFromDbPayload(dbPayload) {
  if (!dbPayload) {
    return null;
  }

  const user = new User();

  user.id = dbPayload.id;
  user.firstName = dbPayload.firstName;
  user.lastName = dbPayload.lastName;
  user.email = dbPayload.email;
  user.password = dbPayload.password;
  user.username = dbPayload.username;
  user.tel = dbPayload.tel;
  user.address = addressFactory.createFromDbPayload(dbPayload.address);
  user.favoris = dbPayload.favoris;

  return user;
}

function createFromPayload(payload) {
  return createFromDbPayload(payload);
}

module.exports = {
  createFromDbPayload,
  createFromPayload,
};
