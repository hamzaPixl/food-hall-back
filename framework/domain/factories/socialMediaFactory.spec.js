const assert = require('assert');
const faker = require('faker');
const socialMediaFactory = require('./socialMediaFactory');

describe('Food Hall factories - socialMediaFactory', () => {
  it('should map a database payload to a social media', () => {
    const dbPayload = {
      source: faker.internet.domainWord(),
      link: faker.internet.url(),
    };

    const socialMediaInfo = socialMediaFactory.createFromDbPayload(dbPayload);

    assert.equal(socialMediaInfo.link, dbPayload.link);
    assert.equal(socialMediaInfo.source, dbPayload.source);
  });

  it('should map a undefined to an empty social media', () => {
    const socialMediaInfo = socialMediaFactory.createFromDbPayload(null);

    assert.equal(socialMediaInfo, null);
  });
});
