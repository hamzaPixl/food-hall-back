const assert = require('assert');
const faker = require('faker');
const restoFactory = require('./restoFactory');

describe('Food Hall factories - restoFactory', () => {
  it('should map a database payload to a resto item', () => {
    const dbPayload = {
      id: faker.random.uuid(),
      tel: faker.phone.phoneNumber(),
      description: faker.lorem.paragraph(),
      title: faker.lorem.words(),
      logo: faker.internet.avatar(),
      likes: faker.random.number(),
      views: faker.random.number(),
      loc: { type: 'Point', coordinates: [parseFloat(faker.address.longitude()), parseFloat(faker.address.latitude())] },
      priceRange: '$$$',
      paymentMethod: [{ type: 'cash' }, { type: 'sodexo' }, { type: 'bancontact' }, { type: 'visa' }],
      address: {
        street: faker.address.streetAddress(),
        number: faker.random.number(),
        city: faker.address.city(),
        zip: faker.address.zipCode(),
        country: faker.address.country(),
      },
      categories: [
        {
          name: faker.random.word(),
        },
        {
          name: faker.random.word(),
        },
        {
          name: faker.random.word(),
        },
      ],
      socialMedias: [
        {
          source: faker.internet.domainWord(),
          link: faker.internet.url(),
        },
        {
          source: faker.internet.domainWord(),
          link: faker.internet.url(),
        },
      ],
      openDays: [
        {
          day: faker.lorem.word(),
          openTime: '8:00 - 15:00',
        },
        {
          day: faker.lorem.word(),
          openTime: '8:00 - 18:00',
        },
        {
          day: faker.lorem.word(),
          openTime: '8:00 - 19:00',
        },

      ],
      createdAt: faker.date.recent(),
      updatedAt: faker.date.recent(),
    };

    const restoInfo = restoFactory.createFromDbPayload(dbPayload);
    assert.equal(restoInfo.createdAt, dbPayload.createdAt);
    assert.equal(restoInfo.updatedAt, dbPayload.updatedAt);
    assert.equal(restoInfo.id, dbPayload.id);
    assert.equal(restoInfo.tel, dbPayload.tel);
    assert.equal(restoInfo.description, dbPayload.description);
    assert.equal(restoInfo.title, dbPayload.title);
    assert.equal(restoInfo.logo, dbPayload.logo);
    assert.equal(restoInfo.likes, dbPayload.likes);
    assert.equal(restoInfo.views, dbPayload.views);
    assert.equal(restoInfo.loc.latitude, dbPayload.loc.latitude);
    assert.equal(restoInfo.loc.longitude, dbPayload.loc.longitude);
    assert.equal(restoInfo.priceRange, dbPayload.priceRange);
  });

  it('should map a undefined to an empty resto item', () => {
    const restoInfo = restoFactory.createFromDbPayload(null);

    assert.equal(restoInfo, null);
  });
});
