const Address = require('../models/Address');

function createFromDbPayload(dbPayload) {
  if (!dbPayload) {
    return null;
  }

  const address = new Address();

  address.street = dbPayload.street || '';
  address.number = dbPayload.number || '';
  address.city = dbPayload.city || '';
  address.zip = dbPayload.zip || '';
  address.country = dbPayload.country || '';

  return address;
}

function createFromPayload(payload) {
  return createFromDbPayload(payload);
}

module.exports = {
  createFromDbPayload,
  createFromPayload,
};
