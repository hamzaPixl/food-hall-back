const OpenDay = require('../models/OpenDay');

function createFromDbPayload(dbPayload) {
  if (!dbPayload) {
    return null;
  }

  const openDay = new OpenDay();

  openDay.day = dbPayload.day || '';
  openDay.openTime = dbPayload.openTime || '';

  return openDay;
}

function createFromPayload(payload) {
  return createFromDbPayload(payload);
}

function createFromDbPayloadMap(payloads) {
  return payloads.map(payload => createFromDbPayload(payload));
}

module.exports = {
  createFromDbPayload,
  createFromPayload,
  createFromDbPayloadMap,
};
