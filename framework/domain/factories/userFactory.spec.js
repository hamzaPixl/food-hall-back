const assert = require('assert');
const faker = require('faker');
const userFactory = require('./userFactory');

describe('Food Hall factories - userFactory', () => {
  it('should map a database payload to a user', () => {
    const dbPayload = {
      id: faker.random.uuid(),
      tel: faker.phone.phoneNumber(),
      email: faker.internet.email(),
      username: faker.internet.userName(),
      password: faker.internet.password(),
      lastName: faker.name.lastName(),
      firstName: faker.name.firstName(),
      favoris: [faker.random.uuid(), faker.random.uuid(), faker.random.uuid(), faker.random.uuid()],
      address: {
        street: faker.address.streetAddress(),
        number: faker.random.number(),
        city: faker.address.city(),
        zip: faker.address.zipCode(),
        country: faker.address.country(),
      },
    };

    const userInfo = userFactory.createFromDbPayload(dbPayload);
    assert.equal(userInfo.id, dbPayload.id);
    assert.equal(userInfo.tel, dbPayload.tel);
    assert.equal(userInfo.email, dbPayload.email);
    assert.equal(userInfo.username, dbPayload.username);
    assert.equal(userInfo.password, dbPayload.password);
    assert.equal(userInfo.lastName, dbPayload.lastName);
    assert.equal(userInfo.firstName, dbPayload.firstName);

    assert.equal(userInfo.address.street, dbPayload.address.street);
    assert.equal(userInfo.address.number, dbPayload.address.number);
    assert.equal(userInfo.address.city, dbPayload.address.city);
    assert.equal(userInfo.address.zip, dbPayload.address.zip);
    assert.equal(userInfo.address.country, dbPayload.address.country);
  });

  it('should map a undefined to an user', () => {
    const userInfo = userFactory.createFromDbPayload(null);

    assert.equal(userInfo, null);
  });
});
