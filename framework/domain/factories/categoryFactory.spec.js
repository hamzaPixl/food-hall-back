const assert = require('assert');
const faker = require('faker');
const categoryFactory = require('./categoryFactory');

describe('Food Hall factories - categoryFactory', () => {
  it('should map a database payload to a category', () => {
    const dbPayload = {
      name: faker.name.jobTitle(),
    };

    const categoryInfo = categoryFactory.createFromDbPayload(dbPayload);

    assert.equal(categoryInfo.name, dbPayload.name);
  });

  it('should map a undefined to an empty category', () => {
    const categoryInfo = categoryFactory.createFromDbPayload(null);

    assert.equal(categoryInfo, null);
  });
});
