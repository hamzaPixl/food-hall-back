const PaymentMethod = require('../models/PaymentMethod');

function createFromDbPayload(dbPayload) {
  if (!dbPayload) {
    return null;
  }

  const paymentMethod = new PaymentMethod();

  paymentMethod.type = dbPayload.type || '';

  return paymentMethod;
}

function createFromPayload(payload) {
  return createFromDbPayload(payload);
}

function createFromDbPayloadMap(payloads) {
  return payloads.map(payload => createFromDbPayload(payload));
}

module.exports = {
  createFromDbPayload,
  createFromPayload,
  createFromDbPayloadMap,
};
