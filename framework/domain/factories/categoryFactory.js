const Category = require('../models/Category');

function createFromDbPayload(dbPayload) {
  if (!dbPayload) {
    return null;
  }

  const category = new Category();

  category.name = dbPayload.name || '';

  return category;
}

function createFromPayload(payload) {
  return createFromDbPayload(payload);
}

function createFromDbPayloadMap(payloads) {
  return payloads.map(payload => createFromDbPayload(payload));
}

module.exports = {
  createFromDbPayload,
  createFromPayload,
  createFromDbPayloadMap,
};
