const Loc = require('../models/Loc');

function createFromDbPayload(dbPayload) {
  if (!dbPayload) {
    return null;
  }

  const loc = new Loc();

  loc.type = dbPayload.type;
  loc.coordinates = dbPayload.coordinates;

  return loc;
}

function createFromPayload(payload) {
  return createFromDbPayload(payload);
}

function createFromDbPayloadMap(payloads) {
  return payloads.map(payload => createFromDbPayload(payload));
}

module.exports = {
  createFromDbPayload,
  createFromPayload,
  createFromDbPayloadMap,
};
