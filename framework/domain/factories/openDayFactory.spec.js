const assert = require('assert');
const faker = require('faker');
const openDayFactory = require('./openDayFactory');

describe('Food Hall factories - openDayFactory', () => {
  it('should map a database payload to an open day', () => {
    const dbPayload = {
      day: faker.lorem.word(),
      openTime: '8:00 - 12:00',
    };

    const openDayInfo = openDayFactory.createFromDbPayload(dbPayload);

    assert.equal(openDayInfo.day, dbPayload.day);
    assert.equal(openDayInfo.openTime, dbPayload.openTime);
  });

  it('should map a undefined to an empty open day', () => {
    const openDayInfo = openDayFactory.createFromDbPayload(null);

    assert.equal(openDayInfo, null);
  });
});
