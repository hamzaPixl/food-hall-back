const assert = require('assert');
const faker = require('faker');
const paymentMethodFactory = require('./paymentMethodFactory');

describe('Food Hall factories - paymentMethodFactory', () => {
  it('should map a database payload to a payment method', () => {
    const dbPayload = {
      type: faker.name.jobTitle(),
    };

    const paymentMethodInfo = paymentMethodFactory.createFromDbPayload(dbPayload);

    assert.equal(paymentMethodInfo.type, dbPayload.type);
  });

  it('should map a undefined to an empty payent method', () => {
    const paymentMethodInfo = paymentMethodFactory.createFromDbPayload(null);

    assert.equal(paymentMethodInfo, null);
  });
});
