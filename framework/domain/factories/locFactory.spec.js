const assert = require('assert');
const faker = require('faker');
const locFactory = require('./locFactory');

describe('Food Hall factories - locFactory', () => {
  it('should map a database payload to a loc', () => {
    const coordinates = [parseFloat(faker.address.longitude()), parseFloat(faker.address.latitude())];
    const dbPayload = {
      type: 'Point',
      coordinates,
    };
    const locInfo = locFactory.createFromDbPayload(dbPayload);

    assert.equal(locInfo.type, dbPayload.type);
    assert.equal(locInfo.coordinates[0], dbPayload.coordinates[0]);
    assert.equal(locInfo.coordinates[1], dbPayload.coordinates[1]);
  });

  it('should map a undefined to an empty loc', () => {
    const locInfo = locFactory.createFromDbPayload(null);

    assert.equal(locInfo, null);
  });
});
