const Resto = require('../models/Resto');
const addressFactory = require('./addressFactory');
const categoryFactory = require('./categoryFactory');
const paymentMethodFactory = require('./paymentMethodFactory');
const openDayFactory = require('./openDayFactory');
const socialMediaFactory = require('./socialMediaFactory');
const locFactory = require('./locFactory');

function createFromDbPayload(dbPayload) {
  if (!dbPayload) {
    return null;
  }

  const resto = new Resto();
  resto.id = dbPayload.id || '';
  resto.tel = dbPayload.tel || '';
  resto.description = dbPayload.description || '';
  resto.title = dbPayload.title || '';
  resto.logo = dbPayload.logo || '';
  resto.likes = dbPayload.likes || 0;
  resto.views = dbPayload.views || 0;
  resto.priceRange = dbPayload.priceRange || '$';
  resto.loc = locFactory.createFromDbPayload(dbPayload.loc);
  resto.address = addressFactory.createFromDbPayload(dbPayload.address);
  resto.categories = categoryFactory.createFromDbPayloadMap(dbPayload.categories);
  resto.paymentMethod = paymentMethodFactory.createFromDbPayloadMap(dbPayload.paymentMethod);
  resto.socialMedias = socialMediaFactory.createFromDbPayloadMap(dbPayload.socialMedias);
  resto.openDays = openDayFactory.createFromDbPayloadMap(dbPayload.openDays);
  resto.createdAt = dbPayload.createdAt;
  resto.updatedAt = dbPayload.updatedAt;

  return resto;
}

function createFromPayload(payload) {
  return createFromDbPayload(payload);
}

function createFromDbPayloadMap(payloads) {
  return payloads.map(payload => createFromDbPayload(payload));
}

module.exports = {
  createFromDbPayload,
  createFromPayload,
  createFromDbPayloadMap,
};
