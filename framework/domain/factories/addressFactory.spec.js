const assert = require('assert');
const faker = require('faker');
const addressFactory = require('./addressFactory');

describe('Food Hall factories - adressFactory', () => {
  it('should map a database payload to an address', () => {
    const dbPayload = {
      street: faker.address.streetAddress(),
      number: faker.random.number(),
      city: faker.address.city(),
      zip: faker.address.zipCode(),
      country: faker.address.country(),
    };

    const addressInfo = addressFactory.createFromDbPayload(dbPayload);

    assert.equal(addressInfo.street, dbPayload.street);
    assert.equal(addressInfo.number, dbPayload.number);
    assert.equal(addressInfo.city, dbPayload.city);
    assert.equal(addressInfo.zip, dbPayload.zip);
    assert.equal(addressInfo.country, dbPayload.country);
  });

  it('should map a undefined to an empty address', () => {
    const addressInfo = addressFactory.createFromDbPayload(null);

    assert.equal(addressInfo, null);
  });
});
