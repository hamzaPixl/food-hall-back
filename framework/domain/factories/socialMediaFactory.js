const SocialMedia = require('../models/SocialMedia');

function createFromDbPayload(dbPayload) {
  if (!dbPayload) {
    return null;
  }

  const socialMedia = new SocialMedia();

  socialMedia.source = dbPayload.source || '';
  socialMedia.link = dbPayload.link || '';

  return socialMedia;
}

function createFromPayload(payload) {
  return createFromDbPayload(payload);
}

function createFromDbPayloadMap(payloads) {
  return payloads.map(payload => createFromDbPayload(payload));
}

module.exports = {
  createFromDbPayload,
  createFromPayload,
  createFromDbPayloadMap,
};
