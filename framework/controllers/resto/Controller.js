const express = require('express');
const { restoServices } = require('../../services');

const router = express.Router();

router.get('/around',
  (req, res, next) => restoServices
    .getAround(req.query, req.app.locals.db)
    .then(result => res.status(200).json(result))
    .catch(next)
);

router.get('/:restoId',
  (req, res, next) => restoServices
    .getById(req.params.restoId, req.app.locals.db)
    .then(result => res.status(200).json(result))
    .catch(next)
);

router.get('/',
  (req, res, next) => restoServices
    .get(req.query, req.app.locals.db)
    .then(result => res.status(200).json(result))
    .catch(next)
);

module.exports = router;
