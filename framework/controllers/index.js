const express = require('express');
const restoController = require('./resto');

const router = express.Router();

router.use('/resto', restoController);

module.exports = router;
