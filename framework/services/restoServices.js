const { restoRepository } = require('../domain/repositories');

function get(query, db) {
  return restoRepository.get(query, db);
}

function getById(restoId, db) {
  return restoRepository.getById(restoId, db);
}

function getAround(coord, db) {
  return restoRepository.getAround(coord, db);
}

module.exports = {
  get,
  getById,
  getAround,
};
